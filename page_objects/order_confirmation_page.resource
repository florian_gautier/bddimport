*** Settings ***    # robocop: disable=missing-doc-suite
Library    String
Library    Collections
Library    Browser
Resource    ../helpers/data_format.resource


*** Variables ***
&{ORDER_CONF_LOCATORS}    confirmation_lbl=//h3[@class="h1 card-title"]
...                       order_reference_txt=//*[@id="order-reference-value"]
...                       confirmation_msg=VOTRE COMMANDE EST CONFIRMÉE
...                       orders_divs=//div[@class="order-line row"]
...                       order_xpath=//div[contains(@class, "details")]/span
...                       total_price=//tr[contains(@class, 'total-value')]/td[2]
...                       product_subtotal=//div[@class="order-confirmation-table"]//tr[1]/td[2]
...                       delivery_price=//div[@class="order-confirmation-table"]//tr[2]/td[2]
...                       unit_price=//div[contains(@class,"qty")]/div/div[1]
...                       number=//div[contains(@class,"qty")]/div/div[2]


*** Keywords ***
# robocop: disable=missing-doc-keyword
Add Row Locator For Products
    [Arguments]    ${products}
    [Documentation]    For a list of products, adds the order row xpath value for each product dictionary.
    ${order_lines} =    Get Element Count    ${ORDER_CONF_LOCATORS}[order_xpath]
    FOR    ${line}    IN RANGE   ${order_lines}
        ${product_name} =    Get Text    ${ORDER_CONF_LOCATORS}[order_xpath] >> nth=${line}
        FOR    ${product}    IN    @{products}
            IF    "${product}[Product]" in "${product_name}"    Set To Dictionary    ${product}    order_line    ${line}
        END
    END
    RETURN    ${products}

Validate Product Quantity And Price
    [Arguments]    ${product}
    Get Text    ${ORDER_CONF_LOCATORS}[unit_price] >> nth=${product}[order_line]    *=    ${product}[UnitPrice]
    Get Text    ${ORDER_CONF_LOCATORS}[number] >> nth=${product}[order_line]   ==    ${product}[Number]

Validate Products Quantities And Prices
    [Arguments]    ${products}
    FOR    ${product}    IN    @{products}
        Validate Product Quantity And Price    ${product}
    END

Total Order Price Including Tax Is ${total_price_input}
    [Documentation]    Retrieves the total in the order summary
    ...                and compares it to the \${total_price_input}.

    ${numeric_total_price_input} =    Replace String    ${total_price_input}    ,    .

    ${site_total_price} =    Retrieve Numeric Value From Xpath
    ...                      ${ORDER_CONF_LOCATORS}[total_price]

    Should Be Equal As Numbers    ${numeric_total_price_input}    ${site_total_price}

Confirmation Message Should Be Displayed
    Get Text    ${ORDER_CONF_LOCATORS}[confirmation_lbl]    *=    ${ORDER_CONF_LOCATORS}[confirmation_msg]
    ${command_reference} =    Get Text    ${ORDER_CONF_LOCATORS}[order_reference_txt]
    ${reference} =    Split String    ${command_reference}
    Set Test Variable    $ORDER_REFERENCE    ${reference}[5]

The Total Order Price Equals The Product Subtotal Plus Delivery Price
    [Arguments]    ${total_price_input}
    [Documentation]    Verifies if the total order price including tax equals the sum of the total
    ...    product price and the delivery price.

    ${delivery} =    Get Text    ${ORDER_CONF_LOCATORS}[delivery_price]

    IF    '${delivery}' == 'gratuit'
        ${delivery_total} =    Set Variable    0
    ELSE
        ${delivery_total} =    Retrieve Numeric Value From Xpath   ${ORDER_CONF_LOCATORS}[delivery_price]
    END

    ${product_subtotal} =        Retrieve Numeric Value From Xpath   ${ORDER_CONF_LOCATORS}[product_subtotal]
    ${order_subtotal} =    Evaluate    round(${delivery_total} + ${product_subtotal}, 2)
    ${total_price} =     Replace String    ${total_price_input}    ,    .

    Should Be Equal As Numbers   ${total_price}    ${order_subtotal}
