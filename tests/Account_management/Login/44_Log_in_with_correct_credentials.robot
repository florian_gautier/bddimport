*** Settings ***
Documentation    Log in with correct credentials
...
...              This test case verifies a customer can successfully log in to his/her 
...              Prestashop account with valid credentials.
Metadata         ID                           44
Metadata         Reference                    ACC_TC_007
Metadata         Automation priority          null
Metadata         Test case importance         Very high
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Log in with correct credentials
    [Documentation]    Log in with correct credentials

    Given I created an account with gender "F" firstName "Mary" lastName "Smith" email "marysmith@example.com" password "smitty4life" birthDate "01/01/2000" partnerOffers "yes" newsletter "yes"
    And I am logged out
    And I am on the "Login" page
    When I log in with email "marysmith@example.com" and password "smitty4life"
    Then I can see firstName and lastName "Mary Smith" in the top right corner


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_44_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_44_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_44_SETUP_VALUE} =    Get Variable Value    ${TEST_44_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_44_SETUP_VALUE is not None
        Run Keyword    ${TEST_44_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_44_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_44_TEARDOWN}.

    ${TEST_44_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_44_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_44_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_44_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END
