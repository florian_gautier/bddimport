*** Settings ***
Documentation    Cannot log in with an incorrect password
...
...              This test case verifies that the login fails if a customer provides an 
...              incorrect password, triggering an associated error message "Échec 
...              d'authentification" (Authentication failure) on the login page.
Metadata         ID                           45
Metadata         Reference                    ACC_TC_008
Metadata         Automation priority          null
Metadata         Test case importance         Very high
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Cannot log in with an incorrect password
    [Documentation]    Cannot log in with an incorrect password

    Given I created an account with gender "F" firstName "Alice" lastName "Noel" email "alice@noel.com" password "police" birthDate "01/01/1970" partnerOffers "yes" newsletter "yes"
    And I am logged out
    And I am on the "Login" page
    When I log in with email "alice@noel.com" and password "poluce"
    Then I should still be on the "Login" page
    And The error message "Échec d'authentification" should be displayed


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_45_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_45_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_45_SETUP_VALUE} =    Get Variable Value    ${TEST_45_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_45_SETUP_VALUE is not None
        Run Keyword    ${TEST_45_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_45_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_45_TEARDOWN}.

    ${TEST_45_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_45_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_45_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_45_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END
