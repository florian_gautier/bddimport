*** Settings ***
Documentation    Cannot add a product to the cart if the quantity exceeds stock
...
...              This test case verifies that the system prevents adding a product to the 
...              cart if the specified quantity exceeds the available stock, displaying a 
...              relevant warning message.
Metadata         ID                           49
Metadata         Reference                    CART_TC_004
Metadata         Automation priority          null
Metadata         Test case importance         Very high
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Cannot add a product to the cart if the quantity exceeds stock
    [Documentation]    Cannot add a product to the cart if the quantity exceeds stock

    Given I am logged out
    And I am on the "Home" page
    When I navigate to category "accessoires"
    And I navigate to product "Mug Today is a good day"
    And I update quantity to "1599"
    Then The warning message "Le stock est insuffisant." should be displayed
    And I should not be able to add to cart


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_49_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_49_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_49_SETUP_VALUE} =    Get Variable Value    ${TEST_49_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_49_SETUP_VALUE is not None
        Run Keyword    ${TEST_49_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_49_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_49_TEARDOWN}.

    ${TEST_49_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_49_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_49_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_49_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END
