*** Settings ***
Documentation    Remove a product from the cart
...
...              This test case verifies the successful removal of a product from the cart 
...              while maintaining the correct display of the remaining products.
Metadata         ID                           52
Metadata         Reference                    CART_TC_007
Metadata         Automation priority          null
Metadata         Test case importance         High
Resource         squash_resources.resource
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Remove a product from the cart
    [Documentation]    Remove a product from the cart

    &{datatables} =    Retrieve Datatables

    Given I am logged in
    And The cart contains "${datatables}[datatable_1]"
    And I am on the "Cart" page
    When I remove product "Mug The best is yet to come"
    Then The cart should contain "${datatables}[datatable_2]"
    And The total number of products should be "1" and the total price should be "10,80"


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_52_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_52_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_52_SETUP_VALUE} =    Get Variable Value    ${TEST_52_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_52_SETUP_VALUE is not None
        Run Keyword    ${TEST_52_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_52_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_52_TEARDOWN}.

    ${TEST_52_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_52_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_52_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_52_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END

Retrieve Datatables
    [Documentation]    Retrieves Squash TM's datatables and stores them in a dictionary.
    ...
    ...                For instance, 2 datatables have been defined in Squash TM,
    ...                the first one containing data:
    ...                | name | firstName |
    ...                | Bob  |   Smith   |
    ...                the second one containing data
    ...                | name  | firstName | age |
    ...                | Alice |   Smith   | 45  |
    ...
    ...                First, for each datatable, this keyword retrieves the values of each row
    ...                and stores them in a list, as follows:
    ...                @{row_1_1} =    Create List    name    firstName
    ...
    ...                Then, for each datatable, this keyword creates a list containing all the rows,
    ...                as lists themselves, as follows:
    ...                @{datatable_1} =    Create List    ${row_1_1}    ${row_1_2}
    ...
    ...                Finally, this keyword stores the datatables into the &{datatables} dictionary
    ...                with each datatable name as key, and each datatable list as value :
    ...                &{datatables} =    Create Dictionary    datatable_1=${datatable_1}    datatable_2=${datatable_2}

    @{row_1_1} =    Create List    Product                            Number
    @{row_1_2} =    Create List    Mug The best is yet to come        5
    @{row_1_3} =    Create List    Illustration vectorielle Renard    1
    @{datatable_1} =    Create List    ${row_1_1}    ${row_1_2}    ${row_1_3}

    @{row_2_1} =    Create List    Product                            Number    UnitPrice
    @{row_2_2} =    Create List    Illustration vectorielle Renard    1         10,80
    @{datatable_2} =    Create List    ${row_2_1}    ${row_2_2}

    &{datatables} =    Create Dictionary    datatable_1=${datatable_1}    datatable_2=${datatable_2}

    RETURN    &{datatables}
