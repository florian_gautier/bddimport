*** Settings ***
Documentation    Place an order - logged-out customer
...
...              This test case verifies the successful order placement by a logged-out 
...              customer, ensuring the order contains specified products.
Metadata         ID                           37
Metadata         Reference                    ORD_TC_003
Metadata         Automation priority          null
Metadata         Test case importance         Very high
Resource         squash_resources.resource
Library          squash_tf.TFParamService
Test Setup       Test Setup
Test Teardown    Test Teardown


*** Test Cases ***
Place an order - logged-out customer
    [Documentation]    Place an order - logged-out customer

    &{dataset} =       Retrieve Dataset
    &{datatables} =    Retrieve Datatables

    Given I created an account with gender "M" firstName "john" lastName "doe" email "johndoe@mail.com" password "pass1234" birthDate "01/01/1950" partnerOffers "yes" newsletter "yes"
    And I am logged out
    And I am on the "Home" page
    When I navigate to category "accessoires"
    And I navigate to product "Mug The best is yet to come"
    And I add to cart
    And I initiate order placement process
    And I fill login form with email "johndoe@mail.com" and password "pass1234"
    And I fill command form with alias "${dataset}[alias]" company "${dataset}[company]" vat "${dataset}[vat]" address "${dataset}[address]" supp "${dataset}[supp]" zip "${dataset}[zip]" city "${dataset}[city]" country "${dataset}[country]" phone "${dataset}[phone]" and facturation "yes" and submit
    And I choose delivery "${dataset}[delivery]" and command message "${dataset}[message]"
    And I pay by paymode "${dataset}[paymode]" and choose approveSalesConditions "yes"
    And I submit order
    Then The order should be placed and it should contain "${datatables}[datatable_1]"
    And The total order price should be "${dataset}[total_price]"


*** Keywords ***
Test Setup
    [Documentation]    test setup
    ...                You can define the ${TEST_SETUP} variable with a keyword for setting up all your tests.
    ...                You can define the ${TEST_37_SETUP} variable with a keyword for setting up this specific test.
    ...                If both are defined, ${TEST_37_SETUP} will be run after ${TEST_SETUP}.

    ${TEST_SETUP_VALUE} =        Get Variable Value    ${TEST_SETUP}
    ${TEST_37_SETUP_VALUE} =    Get Variable Value    ${TEST_37_SETUP}
    IF    $TEST_SETUP_VALUE is not None
        Run Keyword    ${TEST_SETUP}
    END
    IF    $TEST_37_SETUP_VALUE is not None
        Run Keyword    ${TEST_37_SETUP}
    END

Test Teardown
    [Documentation]    test teardown
    ...                You can define the ${TEST_TEARDOWN} variable with a keyword for tearing down all your tests.
    ...                You can define the ${TEST_37_TEARDOWN} variable with a keyword for tearing down this specific test.
    ...                If both are defined, ${TEST_TEARDOWN} will be run after ${TEST_37_TEARDOWN}.

    ${TEST_37_TEARDOWN_VALUE} =    Get Variable Value    ${TEST_37_TEARDOWN}
    ${TEST_TEARDOWN_VALUE} =        Get Variable Value    ${TEST_TEARDOWN}
    IF    $TEST_37_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_37_TEARDOWN}
    END
    IF    $TEST_TEARDOWN_VALUE is not None
        Run Keyword    ${TEST_TEARDOWN}
    END

Retrieve Dataset
    [Documentation]    Retrieves Squash TM's datasets and stores them in a dictionary.
    ...
    ...                For instance, datasets containing 3 parameters "city", "country" and "currency"
    ...                have been defined in Squash TM.
    ...
    ...                First, this keyword retrieves parameter values from Squash TM
    ...                and stores them into variables, using the keyword 'Get Test Param':
    ...                ${city} =    Get Test Param    DS_city
    ...
    ...                Then, this keyword stores the parameters into the &{dataset} dictionary
    ...                with each parameter name as key, and each parameter value as value:
    ...                &{dataset} =    Create Dictionary    city=${city}    country=${country}    currency=${currency}

    ${alias} =          Get Test Param    DS_alias
    ${company} =        Get Test Param    DS_company
    ${vat} =            Get Test Param    DS_vat
    ${address} =        Get Test Param    DS_address
    ${supp} =           Get Test Param    DS_supp
    ${zip} =            Get Test Param    DS_zip
    ${city} =           Get Test Param    DS_city
    ${country} =        Get Test Param    DS_country
    ${phone} =          Get Test Param    DS_phone
    ${delivery} =       Get Test Param    DS_delivery
    ${message} =        Get Test Param    DS_message
    ${paymode} =        Get Test Param    DS_paymode
    ${total_price} =    Get Test Param    DS_total_price

    &{dataset} =    Create Dictionary    alias=${alias}                company=${company}      vat=${vat}            address=${address}
    ...                                  supp=${supp}                  zip=${zip}              city=${city}          country=${country}
    ...                                  phone=${phone}                delivery=${delivery}    message=${message}    paymode=${paymode}
    ...                                  total_price=${total_price}

    RETURN    &{dataset}

Retrieve Datatables
    [Documentation]    Retrieves Squash TM's datatables and stores them in a dictionary.
    ...
    ...                For instance, 2 datatables have been defined in Squash TM,
    ...                the first one containing data:
    ...                | name | firstName |
    ...                | Bob  |   Smith   |
    ...                the second one containing data
    ...                | name  | firstName | age |
    ...                | Alice |   Smith   | 45  |
    ...
    ...                First, for each datatable, this keyword retrieves the values of each row
    ...                and stores them in a list, as follows:
    ...                @{row_1_1} =    Create List    name    firstName
    ...
    ...                Then, for each datatable, this keyword creates a list containing all the rows,
    ...                as lists themselves, as follows:
    ...                @{datatable_1} =    Create List    ${row_1_1}    ${row_1_2}
    ...
    ...                Finally, this keyword stores the datatables into the &{datatables} dictionary
    ...                with each datatable name as key, and each datatable list as value :
    ...                &{datatables} =    Create Dictionary    datatable_1=${datatable_1}    datatable_2=${datatable_2}

    @{row_1_1} =    Create List    Product                        Number    UnitPrice
    @{row_1_2} =    Create List    Mug The best is yet to come    1         14,28
    @{datatable_1} =    Create List    ${row_1_1}    ${row_1_2}

    &{datatables} =    Create Dictionary    datatable_1=${datatable_1}

    RETURN    &{datatables}
