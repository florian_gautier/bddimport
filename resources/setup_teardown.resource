*** Settings ***    # robocop: disable=missing-doc-suite
Library      DateTime
Library    OperatingSystem
Library      Browser
Library      squash_tf.TFParamService
Resource    ../helpers/prestashop_api.resource
Resource    ../page_objects/account_creation_page.resource


*** Variables ***
&{EXAMPLE_CUSTOMER_DATA}         gender=F
...                     first_name=firstname
...                     last_name=lastname
...                     password=4321pwd
...                     email=email@mail.com
...                     birth_date=01/01/1990
...                     offers=yes
...                     policy=yes
...                     newsletter=yes
...                     gdpr=yes

${PRESTASHOP_URL_CUF_REFERENCE}    IT_CUF_presta_url
${PRESTASHOP_BROWSER_ENVIRONMENT_VARIABLE_NAME}     PRESTA_BROWSER_TYPE
&{BROWSERS}    chrome=chromium    chromium=chromium
...            firefox=firefox    webkit=webkit

${URL}    ${EMPTY}
${CUSTOMER_MAIL}    ${EMPTY}
${ORDER_REFERENCE}    ${EMPTY}

${TEST_SETUP}    Basic Setup
${TEST_TEARDOWN}    Delete Customer

${TEST_35_TEARDOWN}    Delete Order
${TEST_36_TEARDOWN}    Delete Order


*** Keywords ***
# robocop: disable=missing-doc-keyword
Basic Setup
    Set Library Search Order    Browser
    ${url_value} =    Get Test Param    ${PRESTASHOP_URL_CUF_REFERENCE}
    Set Test Variable    $URL    ${url_value}
    ${timestamp} =    Get Current Date    result_format=%Y%m%d-%H%M%S%f
    Register Keyword To Run On Failure    Take Screenshot    failure_screen_${timestamp}    fullPage=True
    Open Application

Open Application
    ${browser_type} =    Get Environment Variable    ${PRESTASHOP_BROWSER_ENVIRONMENT_VARIABLE_NAME}

    @{browser_keys} =    Get Dictionary Keys    ${BROWSERS}
    IF    '${browser_type}' not in ${browser_keys}    Fail
    ...   '${browser_type}' is not a known browser, known browsers: ${browser_keys}

    New Browser    browser=${BROWSERS}[${browser_type}]    headless=True
    New Page    url=${URL}
    Set Viewport Size    1500    700

Create An Example Customer
    [Arguments]    ${email}    ${password}    ${random_str}
    [Documentation]    Adds a unique random string to customer's first_name, last_name,
    ...                email and password before creating a new customer to assure a
    ...                unique example customer per each test run.

    ${email_random} =    Catenate    ${email}${random_str}
    &{customer} =    Create Dictionary    gender=${EXAMPLE_CUSTOMER_DATA}[gender]
    ...                               first_name=${EXAMPLE_CUSTOMER_DATA}[first_name]${random_str}
    ...                               last_name=${EXAMPLE_CUSTOMER_DATA}[last_name]${random_str}
    ...                               email=${email_random}
    ...                               password=${password}${random_str}
    ...                               birth_date=${EXAMPLE_CUSTOMER_DATA}[birth_date]
    ...                               offers=${EXAMPLE_CUSTOMER_DATA}[offers]
    ...                               newsletter=${EXAMPLE_CUSTOMER_DATA}[newsletter]
    Setup A New Account    &{customer}
    Store Email After Account Creation    ${email_random}

Delete Customer
    IF    "${CUSTOMER_MAIL}" == "${EMPTY}"    RETURN
    Delete PrestaShop Item    customer    ${CUSTOMER_MAIL}

Delete Order
    IF    "${ORDER_REFERENCE}" == "${EMPTY}"    RETURN
    Delete PrestaShop Item    order    ${ORDER_REFERENCE}
